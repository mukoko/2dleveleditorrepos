﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Similar to the Other tiles menu. This menu is opened by that tiles menu when the user clicks on a tile that has alternative sprites
/// </summary>
public class EditorTileVarientsMenu : Menu<EditorTileVarientsMenu>
{
    [SerializeField]
    private GameObject _tilesHolder;

    [SerializeField]
    private GameObject _templateObject;

    private static TileManager.TileInformation _tileInfo;

    public static void Show(TileManager.TileInformation tileInfo)
    {
        _tileInfo = tileInfo;
        Open();
    }

    public static void Hide()
    {
        Close();
    }

    protected override void Awake()
    {
        base.Awake();
        GetVarient(_tileInfo);           
    }

    //Passes the editor tile that was clicked to the editor menu
    private void ETileClicked(TileManager.TileInformation.Rule rule)
    {
        //Do nothing if the tile you clicked was the current selected E Tile
        if (EditorMenu.Instance.currentEditorTile.Equals(_tileInfo, false, rule.id)) return;
        EditorMenu.Instance.EditorTileSelected(_tileInfo, false, rule.id);
    }

    /// <summary>
    /// Displays all of the varients for a tile
    /// </summary>
    public void GetVarient(TileManager.TileInformation tileInfo)
    {
        foreach (TileManager.TileInformation.Rule rule in tileInfo.ruleset)
        {
            GameObject go = Instantiate(_templateObject);
            go.SetActive(true);
            go.transform.SetParent(_tilesHolder.transform, false);
            go.GetComponent<Image>().sprite = rule.sprite;
            go.GetComponent<Button>().onClick.AddListener(delegate { ETileClicked(rule); });
        }
    }

    //When you change a the main E Tile, update the varients
    public void UpdateVarient(TileManager.TileInformation tileInfo)
    {
        foreach (Transform child in _tilesHolder.transform)
        {
            if (child.gameObject.activeSelf)
                Destroy(child.gameObject);
        }

        GetVarient(tileInfo);
    }
}
