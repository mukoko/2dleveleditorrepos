﻿using System.Collections;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This menu populates itself with all of the levels that the users has created,
/// It allows users to then edit these level, or add additional ones.
/// </summary>
public class EditorLevelSelectMenu : SimpleMenu<EditorLevelSelectMenu>
{

    /// <summary>
    /// Groups a level path with a gameobject, so when the gameobject is pressed, the relevant level path can be found.
    /// </summary>
    private struct LevelEntryPair
    {
        public string path { get; private set; }
        public string name { get; private set; }
        public GameObject entry { get; private set; }

        public LevelEntryPair(string path, string name, GameObject entry)
        {
            this.path = path;
            this.name = name;
            this.entry = entry;
        }
    }

    [SerializeField]
    private InputField _inputField;
             
    [SerializeField]
    private GameObject _entryTemplate;

    [SerializeField]
    private Color _selectedColor; 

    private List<LevelEntryPair> _levelEntries;
    private LevelEntryPair _selectedEntry;
    private List<string> _levels;
    private Color _originalColor;
           
    void Start()
    {
        _levelEntries = new List<LevelEntryPair>();
        _levels = new List<string>();

        GetLevels();
    }

    /// <summary>
    /// The selection menu should be populated with all of the levels found under the Levels folder in the Streaming Assets folder
    /// </summary>
    private void GetLevels()
    {
        _levels.Clear();
        FindFiles(Directory.GetDirectories(Application.streamingAssetsPath + "/Levels"));
    }

    private void FindFiles(string[] directories)
    {
        //The first level that is found should be default be the selected one
        bool firstSelected = true;
        foreach (string directory in directories)
        {
            //Each level has its own folder, look at the files in the folder
            foreach (string file in Directory.GetFiles(directory))
            {
                //Finds the file that is an xml file but not a meta one.
                if (!file.Contains(".meta") && file.Contains(".xml"))
                {
                    //Extract that level's name from the path
                    int indexOfLastBackSlash = file.LastIndexOf(@"\");
                    int indexOfDot = file.LastIndexOf(".");
                    string level = file.Substring(indexOfLastBackSlash + 1, indexOfDot - indexOfLastBackSlash - 1);

                    CreateEntries(file, level, firstSelected);
                    firstSelected = false;
                } 
            }
        }
    }

    //Create the menu items
    private void CreateEntries(string path, string name, bool firstSelected)
    {
        //Create the menu object
        GameObject levelEntry = Instantiate(_entryTemplate);
        levelEntry.SetActive(true);
        levelEntry.GetComponentInChildren<Text>().text = name;
        LevelEntryPair pair = new LevelEntryPair(path, name, levelEntry);
        _levelEntries.Add(pair);

        if (firstSelected)
        {
            _selectedEntry = pair;

            Image image = levelEntry.GetComponent<Image>();
            _originalColor = image.color;
            image.color = _selectedColor;
        }

        //When the user clicks on the button this method should be called
        levelEntry.GetComponent<Button>().onClick.AddListener(delegate { ChangeSelectedLevel(pair); });
        levelEntry.transform.SetParent(_entryTemplate.transform.parent, false);
    }            

    /// <summary>
    /// Create a new level
    /// </summary>
    public void Add()
    {
        string directory = Application.streamingAssetsPath + "/Levels/" + _inputField.text;
        //Shouldn't override an existing directory
        if (!Directory.Exists(directory))
        {
            Directory.CreateDirectory(directory);

            //Create a blank xml file in the newly created directory
            string path = directory + "/" + _inputField.text + ".xml";
            FileStream fs = File.Create(path);
            fs.Close();

            XmlSerializer serializer = new XmlSerializer(typeof(ItemDatabase));
            var encoding = Encoding.GetEncoding("UTF-8");
            StreamWriter stream = new StreamWriter(path, false, encoding);

            serializer.Serialize(stream, new ItemDatabase());
            stream.Close();

            GetLevels();
        }
    }

    public void PlaySelected()
    {
               
    }

    public void EditSelected()
    {
        EditorMenu.Show(_selectedEntry.path, _selectedEntry.name);
    }

    /// <summary>
    /// Change the currently selected level
    /// </summary>
    private void ChangeSelectedLevel(LevelEntryPair pair)
    {
        _selectedEntry.entry.GetComponent<Image>().color = _originalColor;
        _selectedEntry = pair;
        _selectedEntry.entry.GetComponent<Image>().color = _selectedColor;
    }

    //Changes the levels that are displayed depending on the search field
    public void SearchFieldChanged(string text)
    {
        if (text == "")
        {
            foreach (LevelEntryPair levelEntry in _levelEntries)
            {
                if (!levelEntry.entry.activeSelf)
                    levelEntry.entry.SetActive(true);
            }
        }
        else
        {
            foreach (LevelEntryPair levelEntry in _levelEntries)
                levelEntry.entry.SetActive(levelEntry.name.StartsWith(text));
        }
    }
}
