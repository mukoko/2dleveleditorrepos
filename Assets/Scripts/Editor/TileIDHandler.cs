﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TileIDHandler : Editor {

    [MenuItem("Tools/Tile ID Handler/Setter")]
    static void GiveIDs()
    {
        GameObject prefab = (GameObject) AssetDatabase.LoadAssetAtPath("Assets/Prefabs/LevelPrefab.prefab", typeof(GameObject));
        TileManager tManager = prefab.GetComponent<TileManager>();

        foreach (TileManager.TileInformation tileInfo in tManager.tiles)
        {
            tileInfo.id = tManager.tiles.IndexOf(tileInfo) + 1;
            foreach (TileManager.TileInformation.Rule rule in tileInfo.ruleset)
            {
                rule.id = tileInfo.ruleset.IndexOf(rule) + 1;
            }
        }
    }
}
