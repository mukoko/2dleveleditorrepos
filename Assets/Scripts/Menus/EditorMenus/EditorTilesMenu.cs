﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Displays all of the Tiles from TileManager that can be painted onto a level
/// </summary>
public class EditorTilesMenu : SimpleMenu<EditorTilesMenu>
{
    [SerializeField]
    private Button _bkground;

    [SerializeField]
    private Button _props;

    [SerializeField]
    private Button _gridTiles;

    [SerializeField]
    private Button _entities;

    [SerializeField]
    private GameObject _tilesHolder;

    [SerializeField]
    private GameObject _tileTemplate;

    private TileManager.TileType _currentType = TileManager.TileType.None;
    //This is temporary, Everything will eventually use the MenuButton class

    protected override void Awake()
    {
        base.Awake();

        ChangeDisplayedTiles(TileManager.TileType.Background);

        _bkground.onClick.AddListener(delegate { ChangeDisplayedTiles(TileManager.TileType.Background); });
        _props.onClick.AddListener(delegate { ChangeDisplayedTiles(TileManager.TileType.Prop); });
        _gridTiles.onClick.AddListener(delegate { ChangeDisplayedTiles(TileManager.TileType.GridTiles); });
        _entities.onClick.AddListener(delegate { ChangeDisplayedTiles(TileManager.TileType.Entity); });

    }

    //Each Tile in TileManager has a set type, this method displays the Tiles that fall under a given type in the ETilesMenu
    private void ChangeDisplayedTiles(TileManager.TileType tileType)
    {
        if (_currentType == tileType) return;
        _currentType = tileType;

        if (_tilesHolder.transform.childCount > 1)
        {
            foreach (Transform child in _tilesHolder.transform)
            {
                if (child.gameObject.activeSelf)
                    Destroy(child.gameObject);
            }
        }

        foreach (TileManager.TileInformation tileInfo in EditorMenu.Instance.levelManager.tileManager.tiles)
        {
            if (tileInfo.tileType == tileType)
            {
                GameObject go = Instantiate(_tileTemplate);
                go.SetActive(true);
                go.transform.SetParent(_tilesHolder.transform, false);
                go.GetComponent<Image>().sprite = tileInfo.defaultSprite;
                go.AddComponent<TemplateButton>().Initialise(tileInfo);
                go.GetComponent<Button>().onClick.AddListener(delegate { ETileClicked(tileInfo); });
            }
        }
    }

    /// <summary>
    /// When a Tile is clicked, it should be passed to the editor as the currently selected Tile.
    /// </summary>
    private void ETileClicked(TileManager.TileInformation tileInfo)
    {
        if (EditorMenu.Instance.currentEditorTile.Equals(tileInfo, true)) return;

        if (EditorMenu.Instance.currentEditorTile.tileInfo != tileInfo)
        {
            //If the selected sprite has alternative sprites, then open the varients menu and display them
            if (tileInfo.hasRulesets)
            {
                if (EditorTileVarientsMenu.Instance != null)
                    EditorTileVarientsMenu.Instance.UpdateVarient(tileInfo);
                else EditorTileVarientsMenu.Show(tileInfo);
            }
            else EditorTileVarientsMenu.Hide();
        }         

        EditorMenu.Instance.EditorTileSelected(tileInfo, true);
    }

    //The goal of this class is display the different varients that a Tile has when the user hovers over the button with their mouse
    public class TemplateButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public TileManager.TileInformation tileInfo { get; private set; }

        private Image _image;

        private bool _entered;
        private float _timer;
        private int _currentPosition;

        public void Initialise(TileManager.TileInformation tileInfo)
        {
            this.tileInfo = tileInfo;
            _image = GetComponent<Image>();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _entered = true;
        }

        //When the mouse exits the button, then reset the button's sprite
        public void OnPointerExit(PointerEventData eventData)
        {
            _entered = false;
            _timer = 0;
            _currentPosition = 0;
            _image.sprite = tileInfo.defaultSprite;
        }

        //Update the button's sprite to scroll through all of the tiles varients
        void Update()
        {
            if (_entered && tileInfo.hasRulesets)
            {
                _timer += Time.deltaTime;
                if (_timer >= 1f)
                {
                    _timer = 0;
                    _currentPosition += 1;
                    if (_currentPosition >= tileInfo.ruleset.Count)
                        _currentPosition = 0;

                    _image.sprite = tileInfo.ruleset[_currentPosition].sprite;
                }
            }
        }
    }
}

