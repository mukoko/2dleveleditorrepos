﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance { get; private set; }

    public EditorLevelSelectMenu _editorLevelSelectPrefab;
    public EditorMenu _editorMenuPrefab;
    public EditorToolsMenu _editorToolsMenuPrefab;
    public EditorTilesMenu _editorTilesMenuPrefab;
    public EditorTileVarientsMenu _editorTileVarientsMenus;

    private Stack<Menu> _menuStack = new Stack<Menu>();
    public Stack<Menu> menuStack { get { return _menuStack; } }

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        EditorLevelSelectMenu.Show();
    }

    void OnDestroy()
    {
        Instance = null;
    }

    public void CreateInstance<T>() where T : Menu
    {
        var prefab = GetPrefab<T>();
        Instantiate(prefab, transform);
    }

    public void OpenMenu(Menu menu)
    {
        if (!menu._addToStack)
            return;

        if (_menuStack.Count > 0)
        {
            if (_menuStack.Peek() == menu)
                return;

            if (menu._disableMenusUnderneath)
            {
                foreach (var loopMenu in _menuStack)
                {
                    loopMenu.gameObject.SetActive(false);

                    if (loopMenu._disableMenusUnderneath)
                        break;
                }
            }

            var topCanvas = menu.GetComponent<Canvas>();
            var previousCanvas = _menuStack.Peek().GetComponent<Canvas>();
            topCanvas.sortingOrder = previousCanvas.sortingOrder + 1;

        }

        _menuStack.Push(menu);

    }

    public void CloseAllOtherMenus(Menu menu)
    {
        if (_menuStack.Count == 0)
            return;

        foreach (var loopMenu in _menuStack)
        {
            if (menu != loopMenu)
            {
                Destroy(loopMenu.gameObject);
            }
        }

        _menuStack.Clear();
        _menuStack.Push(menu);
    }

    public void CloseMenu(Menu menu)
    {
        if (!menu._addToStack)
        {
            if (menu == null) return;

            if (menu._destroyWhenClosed) Destroy(menu.gameObject);
            else menu.gameObject.SetActive(false);

            return;
        }

        if (_menuStack.Count == 0)
        {
            return;
        }

        if (_menuStack.Peek() != menu)
        {
            return;
        }

        CloseTopMenu();
    }

    private void CloseTopMenu()
    {
        var instance = _menuStack.Pop();

        if (instance._destroyWhenClosed)
            Destroy(instance.gameObject);
        else instance.gameObject.SetActive(false);

        foreach (var menu in _menuStack)
        {
            menu.gameObject.SetActive(true);

            if (menu._disableMenusUnderneath)
                break;
        }

        _menuStack.Peek().OnResume();
    }

    private T GetPrefab<T>() where T : Menu
    {
        var fields = GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
        foreach (var field in fields)
        {
            var prefab = field.GetValue(this) as T;
            if (prefab != null)
                return prefab;
        }

        throw new MissingReferenceException("Prefab not found for type " + typeof(T));
    }
}
