﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This is the menu that can be found on the left hand side of the editor, contains a set of tools that allows users to paint/select/undo/redo etc.
/// </summary>
public class EditorToolsMenu : SimpleMenu<EditorToolsMenu>
{

    [SerializeField]
    private Button _saveButton;

    [SerializeField]
    private Button _hideGrid;

    [SerializeField]
    private Button _undo;

    [SerializeField]
    private Button _redo;

    [SerializeField]
    private Button _selectTool;

    [SerializeField]
    private Button _paintTool;

    [SerializeField]
    private Button _lockBkgroundLayer;

    [SerializeField]
    private Button _lockPropsLayer;

    [SerializeField]
    private Button _lockTileLayer;

    [SerializeField]
    private Button _lockEntityLayer;

    protected override void Awake()
    {
        base.Awake();

        _saveButton.onClick.AddListener(Save);
        _hideGrid.onClick.AddListener(DisplayGrid);
        _undo.onClick.AddListener(Undo);
        _redo.onClick.AddListener(Redo);

        _selectTool.onClick.AddListener(delegate { SetLockRestriction(EditorMenu.MenuRestrictions.SelectToolSelected); });
        _paintTool.onClick.AddListener(delegate { SetLockRestriction(EditorMenu.MenuRestrictions.PaintToolSelected); });
        _lockBkgroundLayer.onClick.AddListener(delegate { SetLockRestriction(EditorMenu.MenuRestrictions.BackgroundLayerLocked); });
        _lockPropsLayer.onClick.AddListener(delegate { SetLockRestriction(EditorMenu.MenuRestrictions.PropsLayerLocked); });
        _lockTileLayer.onClick.AddListener(delegate { SetLockRestriction(EditorMenu.MenuRestrictions.GridTilesLayerLocked); });
        _lockEntityLayer.onClick.AddListener(delegate { SetLockRestriction(EditorMenu.MenuRestrictions.EntityLayerLocked); });
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        EditorTileVarientsMenu.Hide();
    }

    private void Save()
    {
        EditorMenu.Instance.Save();
    }

    private void Undo()
    {
        EditorMenu.Instance.Undo();
    }

    private void Redo()
    {
        EditorMenu.Instance.Redo();
    }
            
    private void SetLockRestriction(EditorMenu.MenuRestrictions restriction)
    {
        bool locked = EditorMenu.Instance.SetLockRestriction(restriction);
        //if (locked) then set button to color else set other color.
    }

    private void DisplayGrid()
    {
        EditorMenu.Instance.DisplayGrid();
    }

    public void RestrictionReleased(EditorMenu.MenuRestrictions restriction)
    {
        switch (restriction)
        {
            case EditorMenu.MenuRestrictions.SelectToolSelected:
                Debug.Log("Select Tool Released");
                //Then the button display should be changed
                break;
            case EditorMenu.MenuRestrictions.PaintToolSelected:
                Debug.Log("Paint Tool Released");
                //Then the button display should be changed
                break;
        }
    }

}
