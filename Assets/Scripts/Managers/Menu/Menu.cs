﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Menu<T> : Menu where T : Menu<T>
{

    public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        Instance = (T)this;
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    protected virtual void OnDestroy()
    {
        Debug.Log("Instance of Menu destroyed " + Instance.name);
        Instance = null;
    }

    protected static void Open()
    {
        if (Instance == null)
            MenuManager.Instance.CreateInstance<T>();
        else
            Instance.gameObject.SetActive(true);

        MenuManager.Instance.OpenMenu(Instance);
    }

    protected static void Close()
    {
        if (Instance == null)
        {
            return;
        }

        MenuManager.Instance.CloseMenu(Instance);
    }

    protected static void CloseOtherMenus()
    {
        if (Instance == null)
        {
            return;
        }

        MenuManager.Instance.CloseAllOtherMenus(Instance);
    }
}

public abstract class Menu : MonoBehaviour
{
    public bool _destroyWhenClosed = true;
    public bool _disableMenusUnderneath = true;
    public bool _addToStack = true;
    protected CanvasGroup _canvasGroup;

    public void OnResume()
    {

    }
}


