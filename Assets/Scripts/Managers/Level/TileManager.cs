﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Malee;

/// <summary>
/// This script holds all of the tiles that are avaliable in the editor
/// They are popularised in Unity's Editor
/// </summary>
public class TileManager : MonoBehaviour
{
    public enum TileType
    {
        None, Background, Prop, GridTiles, Entity
    }

    [System.Serializable]
    public class TileInformation
    {
        [SerializeField]
        private int _id;

        [SerializeField]
        private TileType _tileType;

        [SerializeField]
        private Sprite _defaultSprite;

        //Whether the tile has varient sprites
        [SerializeField]
        private bool _hasRulesets;

        [SerializeField]
        private bool _addCollider;

        //The varying sprites
        [Reorderable]
        [SerializeField]
        private RuleList _ruleset;

        private int _displayid;


        public int id { get { return _id; } set { _id = value; } }
        public TileType tileType { get { return _tileType; } }
        public Sprite defaultSprite { get { return _defaultSprite; } }
        public bool hasRulesets { get { return _hasRulesets; } }
        public bool addCollider { get { return _addCollider; } }
        public List<Rule> ruleset { get { return _ruleset.ToList(); } }

        public Rule GetRule(int id)
        {
            return _ruleset.ToList().Find(rule => rule.id == id);
        }

        [System.Serializable]
        private class RuleList : ReorderableArray<Rule> { }

        [System.Serializable]
        public class Rule
        {
            [SerializeField]
            private int _id;

            [SerializeField]
            private string _rule;

            [SerializeField]
            private string _indexes;

            [SerializeField]
            private Sprite _sprite;

            public int id { get { return _id; }  set { _id = value; } }
            public string rule { get { return _rule; } }
            public string indexes { get { return _indexes; } }
            public Sprite sprite { get { return _sprite; } }
        }
    }

    [Reorderable]
    [SerializeField]
    private TileInformationList _gameTiles;
                
    [System.Serializable]
    public class TileInformationList : ReorderableArray<TileInformation> { }

    public List<TileInformation> tiles { get { return _gameTiles.ToList(); } }

    public TileInformation GetTile(int id)
    {
        return _gameTiles.ToList().Find(tile => tile.id == id);
    }

    public List<TileInformation.Rule> GetRuleset(int id)
    {
        TileInformation tile = GetTile(id);
        if (tile != null) return tile.ruleset;
        return null;
    }
}

