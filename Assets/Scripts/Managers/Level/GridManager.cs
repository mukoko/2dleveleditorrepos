﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GridManager : MonoBehaviour
{
    [SerializeField]
    private Sprite _defaultSprite;

    public int defaultIndex { get; set; }

    public List<GridCell> gridCells { get; private set; }

    void Awake()
    {
        defaultIndex = 2;

        gridCells = new List<GridCell>();

        foreach (Transform child in transform)
        {
            GridCell cell = child.GetComponent<GridCell>();
            if (cell != null)
            {
                gridCells.Add(cell);
                cell.gridManager = this;
                cell.layerIndex = defaultIndex;
            }
        }
    }

    public void AddTileToCell(GridCell cell, TileManager.TileInformation tileInfo, bool followsRules, int ruleid)
    {
        cell.ChangeCellTile(tileInfo, followsRules, ruleid);
    }

    public void RefreshCell(int x, int y)
    {
        GridCell cell = GetCell(x, y);
        if (cell != null)
            cell.ChangeTileSprite(x, y);
    }

    public void RefreshAll()
    {
        foreach (GridCell cell in gridCells)
        {
            if (cell.followsRule)
                RefreshCell(cell.x, cell.y);
        }
    }

    public GridCell GetCell(int x, int y)
    {
        return gridCells.Find(c => c.x == x && c.y == y);
    }

    public void RemoveCell(GridCell cell)
    {
        cell.tileInfo = null;
        cell.sRenderer.sprite = _defaultSprite;
        cell.tileID = 0;
        cell.ruleID = 0;
        cell.followsRule = false;
        cell.hasCollider = false;
        cell.layerIndex = defaultIndex;
        cell.RefreshNeighbourTileSprite(cell.x, cell.y);
    }

    public GridCell MoveCell (int x, int y, GridCell cell)
    {
        GridCell foundCell = gridCells.Find(c => c.x == cell.x + x && c.y == cell.y + y);
        if (foundCell != null)
        {
            foundCell.ChangeCellTile(cell.tileInfo, cell.followsRule, cell.ruleID);
            foundCell.hasCollider = cell.hasCollider;
            foundCell.layerIndex = cell.layerIndex;
        }

        return foundCell;
    }

    public void DecreaseIndex(GridCell cell)
    {
        if (cell.layerIndex > defaultIndex)
            cell.ChangeLayer(cell.layerIndex - 1);
    }

    public void IncreaseIndex(GridCell cell)
    {
        cell.ChangeLayer(cell.layerIndex + 1);
    }
}