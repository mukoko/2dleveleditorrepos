﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;


public class XMLManager : MonoBehaviour
{
    private ItemDatabase _itemDatabase;
    public ItemDatabase itemDatabase;

    private LevelManager _levelManager;

    private string _path;

    void Awake()
    {
        ResetDatabase();
    }

    public void Initialise(LevelManager levelManager, string path)
    {
        _levelManager = levelManager;
        _path = path;
    }

    /// <summary>
    /// Overwrites the xml file that contains the information for the current level
    /// Writes all of the levels objects into file
    /// </summary>
    public void Save()
    {
        ResetDatabase();
        FillDatabase();

        XmlSerializer serializer = new XmlSerializer(typeof(ItemDatabase));
        var encoding = Encoding.GetEncoding("UTF-8");
        StreamWriter stream = new StreamWriter(_path, false, encoding);

        serializer.Serialize(stream, _itemDatabase);
        stream.Close();
    }

    /// <summary>
    /// Reads each object in the xml file and creates the respective object for it
    /// </summary>
    public void Load()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ItemDatabase));

        StreamReader stream = new StreamReader(_path);
        _itemDatabase = (ItemDatabase)serializer.Deserialize(stream);
        stream.Close();

        if (_itemDatabase.backgroundEntry != null)
        {
            _levelManager.backgroundManager.layerIndex = _itemDatabase.backgroundEntry.layerIndex;
            _levelManager.backgroundManager.SetBackground(GetTile(_itemDatabase.backgroundEntry.imageID));
        }

        foreach (PropsEntry entry in _itemDatabase.props)
        {
            _levelManager.propManager.AddProp(new Vector2(entry.x, entry.y), GetTile(entry.tileID), entry.layerIndex, entry.hasCollider);
        }

        foreach (GridTileEntry entry in _itemDatabase.gridTiles)
        {
            GridCell cell = _levelManager.gridManager.GetCell(entry.x, entry.y);
            cell.ChangeCellTile(GetTile(entry.tileID), entry.followsRules, entry.ruleID);
            cell.ChangeLayer(entry.layerIndex);
            cell.hasCollider = entry.hasCollider;
        }

        _levelManager.gridManager.RefreshAll();

        foreach (EntityEntry entry in _itemDatabase.entities)
        {
            _levelManager.entityManager.AddEntity(new Vector2(entry.x, entry.y), GetTile(entry.tileID), entry.layerIndex, entry.hasCollider);
        }
    }


    private TileManager.TileInformation GetTile(int id)
    {
        return _levelManager.tileManager.tiles.Find(tile => tile.id == id);
    }

    /// <summary>
    /// Clear Database
    /// </summary>
    private void ResetDatabase()
    {
        _itemDatabase = new ItemDatabase();
    }

    /// <summary>
    /// Goes through each level object and inserts the necessary information to the Item Database object
    /// </summary>
    private void FillDatabase()
    {
        _itemDatabase.backgroundEntry = new BackgroundEntry(_levelManager.backgroundManager.imageID, _levelManager.backgroundManager.layerIndex);

        foreach (PropManager.PropInformation entry in _levelManager.propManager.props)
        {
            Vector3 pos = entry.gameObject.transform.position;
            _itemDatabase.props.Add(new PropsEntry(pos.x, pos.y, entry.layerIndex, entry.tileID, entry.hasCollider));
        }

        foreach (GridCell cell in _levelManager.gridManager.gridCells)
        {
            if (cell.tileInfo != null)
                _itemDatabase.gridTiles.Add(new GridTileEntry(cell.x, cell.y, cell.layerIndex, cell.tileID, cell.ruleID, cell.followsRule, cell.hasCollider));
        }

        foreach (EntityManager.EntityInformation entry in _levelManager.entityManager.entities)
        {
            Vector3 pos = entry.gameObject.transform.position;
            _itemDatabase.entities.Add(new EntityEntry(pos.x, pos.y, entry.layerIndex, entry.tileID, entry.hasCollider));
        }
    }
}

/// <summary>
/// The following classes declare what information is stored into the xml file for each tile type in a level
/// </summary>
public class BackgroundEntry
{
    public BackgroundEntry() { }

    public BackgroundEntry(int id, int index)
    {
        imageID = id;
        layerIndex = index;
    }

    public int imageID;
    public int layerIndex;
}

public class PropsEntry
{
    public PropsEntry() { }

    public PropsEntry(float x, float y, int index, int id, bool hCollider)
    {
        this.x = x;
        this.y = y;
        layerIndex = index;
        tileID = id;
        hasCollider = hCollider;
    }

    public float x;
    public float y;
    public int layerIndex;
    public int tileID;
    public bool hasCollider;
}

public class GridTileEntry
{
    public GridTileEntry() { }

    public GridTileEntry(int x, int y, int index, int id, int rID, bool fRules, bool hCollider)
    {
        this.x = x;
        this.y = y;
        layerIndex = index;
        tileID = id;
        ruleID = rID;
        followsRules = fRules;
        hasCollider = hCollider;
    }

    public int x;
    public int y;
    public int layerIndex;
    public int tileID;
    public int ruleID;
    public bool followsRules;
    public bool hasCollider;
}

public class EntityEntry
{
    public EntityEntry() { }

    public EntityEntry(float x, float y, int index, int id, bool hCollider)
    {
        this.x = x;
        this.y = y;
        layerIndex = index;
        tileID = id;
        hasCollider = hCollider;
    }

    public float x;
    public float y;
    public int layerIndex;
    public int tileID;
    public bool hasCollider;
}

/// <summary>
/// The database holds all of this information in an object, which is passed to the xml file
/// </summary>
public class ItemDatabase
{

    public ItemDatabase()
    {

    }

    public BackgroundEntry backgroundEntry;

    [XmlArray("LevelProps")]
    public List<PropsEntry> props = new List<PropsEntry>();

    [XmlArray("LevelTiles")]
    public List<GridTileEntry> gridTiles = new List<GridTileEntry>();

    [XmlArray("LevelEntities")]
    public List<EntityEntry> entities = new List<EntityEntry>();
}