﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorMenu : Menu<EditorMenu>
{

    /// <summary>
    /// If an enum value is added to the MenuRestriction list, then the user shouldn't be able to do the action
    /// </summary>
    public enum MenuRestrictions
    {
        BackgroundLayerLocked, PropsLayerLocked, GridTilesLayerLocked, EntityLayerLocked, PaintToolSelected, SelectToolSelected
    }

    /// <summary>
    /// This struct represents the current Tile that the user had clicked on in either the EditorTilesMenu or the Varients Menu
    /// </summary>
    public struct EditorTile
    {
        /// <summary>
        /// TileInfo is taken from the TileManager
        /// Follows rules = it will automatically change its sprite to match surronding tiles of the same type
        /// Rule id = if a varient was used rather than a sprite, get the id of the varient
        /// </summary>
        public EditorTile(TileManager.TileInformation tileInfo, bool followRules, int ruleid = 0)
        {
            this.tileInfo = tileInfo;
            this.followRules = followRules;
            this.ruleid = ruleid;
        }

        public TileManager.TileInformation tileInfo { get; private set; }
        public bool followRules { get; private set; }
        public int ruleid { get; private set; }

        public bool Equals(TileManager.TileInformation tileInfo, bool followRules, int ruleid = 0)
        {
            return (this.tileInfo == tileInfo && this.followRules == followRules && this.ruleid == ruleid);
        }

        public void Clear()
        {
            tileInfo = null;
            followRules = false;
            ruleid = 0;
        }

        public void Set(TileManager.TileInformation tileInfo, bool followRules, int ruleid)
        {
            this.tileInfo = tileInfo;
            this.followRules = followRules;
            this.ruleid = ruleid;
        }
    }

    [SerializeField]
    private GameObject _levelTemplate;

    [SerializeField]
    private float _drawCooldown = 0.5f;
    private float _drawTimer;

    [SerializeField]
    private Texture _blankTexture;

    [SerializeField]
    private GameObject _savepanel;

    private static string _levelname;
    private static string _levelpath;

    private GameObject _levelObject;
    public LevelManager levelManager { get; private set; }

    public EditorTile currentEditorTile { get; private set; }

    private List<MenuRestrictions> _currentRestrictions;

    private bool _shiftMultiPaint;

    //Start/End position of mouse when drag (shift) selecting/paint
    private Vector3 _startPos;
    private Vector3 _endPos;

    public List<GameObject> selectedObjectsList { get; private set; }

    private bool _shiftMultiSelect;

    private GUIStyle currentStyle = null;

    public static void Show(string levelpath, string levelname)
    {
        _levelname = levelname;
        _levelpath = levelpath;
        Open();
    }

    public static void Hide()
    {
        EditorToolsMenu.Close();
        EditorTileVarientsMenu.Close();
        EditorTilesMenu.Close();
        Close();
    }

    protected override void Awake() 
    {
        base.Awake();

        //Add the level
        _levelObject = Instantiate(_levelTemplate);
        levelManager = _levelObject.GetComponent<LevelManager>();
        levelManager.Initialise(_levelpath, _levelname);

        selectedObjectsList = new List<GameObject>();
        _currentRestrictions = new List<MenuRestrictions>();

        EditorToolsMenu.Show();
        EditorTilesMenu.Show();
    }

    #region Tile (Varients) Menu
    /// <summary>
    /// Called by the EditorTilesMenu and Varients menu, when you click on a Tile button on their respective menus
    /// This method updates the currently selected Editor Tile to match the one the user just pressed.
    /// 
    /// TileInfo = the information of the tile button you just pressed
    /// RuleFollow = If you selected it from the tilesmenu - true from varients - false
    /// Ruleid is not 0 if you selected it from the varients menu
    /// </summary>
    public void EditorTileSelected(TileManager.TileInformation tileInfo, bool ruleFollow, int ruleid = 0)
    {
        currentEditorTile = new EditorTile(tileInfo, ruleFollow, ruleid);

        //If the tile button you pressed had a type == background then instantly change the level's background
        if (tileInfo.tileType == TileManager.TileType.Background)
        {
            //Unless the background layer has been locked.
            if (_currentRestrictions.Contains(MenuRestrictions.BackgroundLayerLocked))
                currentEditorTile.Clear();
            else
                levelManager.levelEditManager.SetBackground(tileInfo);
        }
    }
    #endregion

    #region Tools Menu
    /// <summary>
    /// When escape is pressed
    /// If the level doesn't need to be saved, then exit the editor
    /// Else show the save panel
    /// </summary>
    private void CloseDialogue()
    {
        if (levelManager.levelEditManager.historyManager.saved)
        {
            Destroy(_levelObject);
            Hide();
        }
        else _savepanel.SetActive(true);

    }

    /// <summary>
    /// Button on the save panel
    /// Exit without saving
    /// </summary>
    public void Yes()
    {
        Destroy(_levelObject);
        Hide();
    }

    /// <summary>
    /// Button on the save panel
    /// Close the save panel
    /// </summary>
    public void No()
    {
        _savepanel.SetActive(false);
    }

    /////////////////
    //The following called by buttons on the EditorToolsMenu

    /// <summary>
    /// Update the xml file for the current level
    /// </summary>
    public void Save()
    {
        levelManager.levelEditManager.SerializeLevel();
        levelManager.levelEditManager.historyManager.saved = true;
    }

    public void DisplayGrid()
    {
        levelManager.levelEditManager.DisplayGrid();
    }

    public void Undo()
    {
        levelManager.levelEditManager.historyManager.Undo();
    }

    public void Redo()
    {
        levelManager.levelEditManager.historyManager.Redo();
    }

    /// <summary>
    /// Called from the Tools Menu when you click on select/paint or a layer lock
    /// Adds or removes the restriction depending on whether it is in the _currentRestrictions list
    /// </summary>
    public bool SetLockRestriction(MenuRestrictions restriction)
    {
        if (_currentRestrictions.Contains(restriction))
        {
            _currentRestrictions.Remove(restriction);

            //Unselect all the currently selected tiles
            if (restriction == MenuRestrictions.SelectToolSelected)
                selectedObjectsList.Clear();
            return false;
        }
        else
        {
            _currentRestrictions.Add(restriction);
            ReleaseRestriction(restriction); 
            return true;
        }
    }

    /// <summary>
    /// If you click on the paint tool, it must be made sure that the select tool isn't selected.
    /// If it is then we should release the restriction.
    /// Works the same for clicking on the select tool.
    /// </summary>
    void ReleaseRestriction(MenuRestrictions restriction)
    {
        switch (restriction)
        {
            case MenuRestrictions.PaintToolSelected:
                if (_currentRestrictions.Contains(MenuRestrictions.SelectToolSelected))
                {
                    _currentRestrictions.Remove(MenuRestrictions.SelectToolSelected);
                    EditorToolsMenu.Instance.RestrictionReleased(MenuRestrictions.SelectToolSelected);
                }
                break;

            case MenuRestrictions.SelectToolSelected:
                if (_currentRestrictions.Contains(MenuRestrictions.PaintToolSelected))
                {
                    _currentRestrictions.Remove(MenuRestrictions.PaintToolSelected);
                    EditorToolsMenu.Instance.RestrictionReleased(MenuRestrictions.PaintToolSelected);
                }
                break;
        }
    }
    #endregion

    void Update()
    {
        //When user wishes to exit the editor
        if (Input.GetKeyDown(KeyCode.Escape))
            CloseDialogue();
        
        //If the save panel is open, then none of the following actions should take place
        if (_savepanel.activeSelf) return;

        //Update the cooldown on drawing
        //The reason for the cooldown is so you do not draw multiple props/entities in a very small period of time
        _drawTimer += Time.deltaTime;

        if (selectedObjectsList.Count > 0)
            SelectObjectsUpdate();

        //If the paint tool is selected then paint, else if the select tool the allow selections
        //Both will not be active at the same time
        if (_currentRestrictions.Contains(MenuRestrictions.PaintToolSelected))
            PaintToolUpdate();
        else if (_currentRestrictions.Contains(MenuRestrictions.SelectToolSelected))
            SelectToolUpdate();
    }

    #region Paint Tool Update
    /// <summary>
    /// Contains the information on the current and previous Tile placed into a grid cell, for the undo/redo functionality
    /// </summary>
    public class TileOldInfoPair
    {
        public GridCell cell { get; private set; }
        public EditorTile oldEditorTile { get; private set; }
        public EditorTile newEditorTile { get; private set; }

        public TileOldInfoPair(GridCell cell, EditorTile oldEditorTile, EditorTile newEditorTile)
        {
            this.cell = cell;
            this.oldEditorTile = oldEditorTile;
            this.newEditorTile = newEditorTile;
        }
    }

    void PaintToolUpdate()
    {
        //When using the paint tool, the select tool should be disabled, and the selected objects must be de-selected
        selectedObjectsList.Clear();

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown(0))
            PaintShiftDown();
        else if (Input.GetMouseButtonUp(0) && _shiftMultiPaint)
            PaintShiftUp();

        else if (_drawTimer >= _drawCooldown && Input.GetMouseButton(0) && !_shiftMultiPaint)
            PaintNormal();
    }

    /// <summary>
    /// If paint is selected and shift button is down
    /// _startPos = The start corner (Point A) of where the drag paint begins
    /// </summary>
    void PaintShiftDown()
    {
        //Do nothing if a tile/prop/entity to paint hasn't been selected
        if (currentEditorTile.tileInfo == null) return;

        _shiftMultiPaint = true;
        _startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    /// <summary>
    /// When the mouse button is released will shifting
    /// The editor should paint the cells between the _start pos and _end pos to a selected tile
    /// </summary>
    void PaintShiftUp()
    {
        _shiftMultiPaint = false;

        if (currentEditorTile.tileInfo == null) return;
        if (currentEditorTile.tileInfo.tileType != TileManager.TileType.GridTiles) return;
        if (_currentRestrictions.Contains(MenuRestrictions.GridTilesLayerLocked)) return;
    
        //Get the position where the mouse was released (Point B)
        _endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Get the colliders of all the objects within the rectangle that is formed from Corner A to Corner B.
        Collider2D[] collidingObjects = Physics2D.OverlapAreaAll(_startPos, _endPos);
        if (collidingObjects == null) return;

        List<TileOldInfoPair> addHistory = new List<TileOldInfoPair>();
        //Iterate through each collider, look if they have a GridCell component if so you should draw the current tile onto it.
        foreach (Collider2D collider in collidingObjects)
        {
            GridCell cell = collider.gameObject.GetComponent<GridCell>();
            if (cell != null)
            {
                addHistory.Add(new TileOldInfoPair(cell, new EditorTile(cell.tileInfo, cell.followsRule, cell.ruleID), currentEditorTile));
                levelManager.levelEditManager.AddTileToCell(cell, currentEditorTile.tileInfo, currentEditorTile.followRules, currentEditorTile.ruleid);
            }
        }

        if (addHistory.Count > 0)
            levelManager.levelEditManager.historyManager.PaintedMultiTiles(addHistory);      
    }

    void PaintNormal()
    {
        //If no tile has been selected return.
        if (currentEditorTile.tileInfo == null) return;

        _drawTimer = 0.0f;
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D collidingObject = Physics2D.OverlapPoint(mousePosition);

        if (collidingObject == null) return; 
        
        //Check if the object that was found is part of the level.
        if (collidingObject.transform.root.tag.Equals("LevelPrefab"))
        {
            //No point in paint the same grid cell tile onto of another
            GridCell cell = collidingObject.GetComponent<GridCell>();
            if (currentEditorTile.Equals(new EditorTile(cell.tileInfo, cell.followsRule, cell.ruleID))) return;

            //Depending of the type that the current E tile is and that the layer of that type is not locked then the appropriate sprite/tile will be drawn.
            if (currentEditorTile.tileInfo.tileType == TileManager.TileType.GridTiles
                && !_currentRestrictions.Contains(MenuRestrictions.GridTilesLayerLocked))
            {
                levelManager.levelEditManager.historyManager.PaintedTile(cell, new EditorTile(cell.tileInfo, cell.followsRule, cell.ruleID), currentEditorTile);
                levelManager.levelEditManager.AddTileToCell(cell, currentEditorTile.tileInfo, currentEditorTile.followRules, currentEditorTile.ruleid);
            }
            else if (currentEditorTile.tileInfo.tileType == TileManager.TileType.Prop
                && !_currentRestrictions.Contains(MenuRestrictions.PropsLayerLocked))
            {
                PropManager.PropInformation info = levelManager.levelEditManager.AddProp(mousePosition, currentEditorTile.tileInfo);
                levelManager.levelEditManager.historyManager.PaintedProp(info);
            }
            else if (currentEditorTile.tileInfo.tileType == TileManager.TileType.Entity
                && !_currentRestrictions.Contains(MenuRestrictions.EntityLayerLocked))
            {
                EntityManager.EntityInformation info = levelManager.levelEditManager.AddEntity(mousePosition, currentEditorTile.tileInfo);
                levelManager.levelEditManager.historyManager.PaintedEntity(info);
            }
        }   
    }
    #endregion

    #region Select Tool Update
    private void SelectToolUpdate()
    {
        //The selectedObjectsList is passed to the OnGUI function, which is used to draw the transparent colored boxes to highlight which cells have been selected.

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown(0))
            SelectShiftDown();
        else if (Input.GetMouseButtonUp(0) && _shiftMultiSelect)
            SelectShiftUp();

        else if (Input.GetKey(KeyCode.LeftControl))
        {
            if (Input.GetMouseButtonDown(0))
                MouseSelect(false);
        }

        else if (Input.GetMouseButtonDown(0))
            MouseSelect(true);
    }

    void SelectShiftDown()
    {
        _shiftMultiSelect = true;
        _startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void SelectShiftUp()
    {
        _shiftMultiSelect = false;

        _endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D[] collidingObjects = Physics2D.OverlapAreaAll(_startPos, _endPos);

        //Works in a similar fashion to how the paint tool does. Up til this point.
        if (collidingObjects == null) return;

        //Old selected objects
        List<GameObject> oldSelected = new List<GameObject>();
        oldSelected.AddRange(selectedObjectsList);

        foreach (Collider2D collider in collidingObjects)
        {
            //Doesn't want to add Grid Cells that do not have a tile added to them. Or if the tile layer is locked
            if (collider.GetComponent<GridCell>() != null && !_currentRestrictions.Contains(MenuRestrictions.GridTilesLayerLocked))
            {
                if (collider.GetComponent<GridCell>().tileInfo != null)
                {
                    //Do not add a cell if it has already been selected.
                    if (!selectedObjectsList.Contains(collider.gameObject))
                        selectedObjectsList.Add(collider.gameObject);
                }
            }
            else if ((collider.GetComponent<PropManager.PropInformation>() != null && !_currentRestrictions.Contains(MenuRestrictions.PropsLayerLocked))
                || (collider.GetComponent<EntityManager.EntityInformation>() != null && !_currentRestrictions.Contains(MenuRestrictions.EntityLayerLocked)))
            {
                if (!selectedObjectsList.Contains(collider.gameObject))
                    selectedObjectsList.Add(collider.gameObject);
            }
        }

        List<GameObject> newSelected = new List<GameObject>();
        newSelected.AddRange(selectedObjectsList);

        //Only add history if at least one object is a prop/cell/entity
        if (collidingObjects.Any(t => t.GetComponent<PropManager.PropInformation>() != null)
            || collidingObjects.Any(t => t.GetComponent<GridCell>() != null && t.GetComponent<GridCell>().tileInfo != null)
            || collidingObjects.Any(t => t.GetComponent<EntityManager.EntityInformation>() != null))
            levelManager.levelEditManager.historyManager.SelectedObject(oldSelected, newSelected);

    }

    void MouseSelect(bool clearList)
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D[] collidingObjects = Physics2D.OverlapPointAll(mousePosition);

        if (collidingObjects == null) return;
        
        GameObject cellObject = null;
        List<GameObject> pObjects = new List<GameObject>();
        List<GameObject> eObjects = new List<GameObject>();

        foreach (Collider2D obj in collidingObjects)
        {
            if (obj.GetComponent<GridCell>() != null)
                cellObject = obj.gameObject;
            else if (obj.GetComponent<PropManager.PropInformation>() != null)
                pObjects.Add(obj.gameObject);
            else if (obj.GetComponent<EntityManager.EntityInformation>() != null)
                eObjects.Add(obj.gameObject);
        }

        if (cellObject == null) return;

        //Gets the prop/entity that has the highest rendering order
        if (pObjects.Count > 0)
            pObjects.OrderByDescending(t => t.GetComponent<PropManager.PropInformation>().layerIndex);

        if (eObjects.Count > 0)
            eObjects.OrderByDescending(t => t.GetComponent<EntityManager.EntityInformation>().layerIndex);

        List<GameObject> oldSelected = new List<GameObject>();
        oldSelected.AddRange(selectedObjectsList);

        //If I clear this here then the selected object will be removed if I click on an empty tile.
        if (clearList) selectedObjectsList.Clear();

        GameObject go = null;
        int goIndex = 0;

        //Set the object to select to be the grid cell, if the grid layer is not locked
        GridCell cell = cellObject.GetComponent<GridCell>();
        if (cell.tileInfo != null && !_currentRestrictions.Contains(MenuRestrictions.GridTilesLayerLocked))
        {
            go = cellObject;
            goIndex = cell.layerIndex;
        }
                        
        //If there was at least one entity found at the mouse position
        if (eObjects.Count > 0)
        {
            //If the entity layer is not locked
            if (!_currentRestrictions.Contains(MenuRestrictions.EntityLayerLocked))
            {
                //Get the entity with the highest sorting order
                EntityManager.EntityInformation eInfo = eObjects[0].GetComponent<EntityManager.EntityInformation>();
                //Change the selected object if the sorted order is greater than the current selected objects one
                if (go == null || eInfo.layerIndex > goIndex)
                {
                    go = eObjects[0];
                    goIndex = eInfo.layerIndex;
                }
            }                                    
        }

        if (pObjects.Count > 0)
        {
            if (!_currentRestrictions.Contains(MenuRestrictions.PropsLayerLocked))
            {
                PropManager.PropInformation pInfo = pObjects[0].GetComponent<PropManager.PropInformation>();
                if (go == null || pInfo.layerIndex > goIndex)
                {
                    go = pObjects[0];
                    goIndex = pInfo.layerIndex;
                }
            }
        }

        //(De-)Select the object depending on whether it is already selected
        if (go != null)
        {
            if (selectedObjectsList.Contains(go))
                selectedObjectsList.Remove(go);
            else selectedObjectsList.Add(go);
        }

        List<GameObject> newSelected = new List<GameObject>();
        newSelected.AddRange(selectedObjectsList);

        if (cellObject.GetComponent<GridCell>() != null || pObjects.Count > 0 || eObjects.Count > 0)
            levelManager.levelEditManager.historyManager.SelectedObject(oldSelected, newSelected);                  
    }
    #endregion

    #region Select Functions
    void SelectObjectsUpdate()
    {
        int x = 0;
        int y = 0;

        if (Input.GetKeyDown(KeyCode.UpArrow))
            y = 1;
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            y = -1;

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            x = -1;
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            x = 1;

        if (x != 0 || y != 0)
            SelectMovement(x, y);

        if (x == 0 && y == 0)
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                SelectDelete();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                foreach (GameObject obj in selectedObjectsList)
                    levelManager.levelEditManager.DecreaseLayerIndex(obj);
                levelManager.levelEditManager.historyManager.DecreasedLayerIndex(selectedObjectsList);
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                foreach (GameObject obj in selectedObjectsList)
                    levelManager.levelEditManager.IncreaseLayerIndex(obj);
                levelManager.levelEditManager.historyManager.IncreasedLayerIndex(selectedObjectsList);
            }
        }
    }

    void SelectMovement(int x, int y)
    {
        List<GameObject> toKeep = new List<GameObject>();
        List<GameObject> oldObj = new List<GameObject>();

        List<object[]> movedObjects = new List<object[]>();
        foreach (GameObject obj in selectedObjectsList)
        {
            float oldX = 0;
            float oldY = 0;
            float newX = 0;
            float newY = 0;
            GridCell newCell;
            EditorTile oldEditorTile = new EditorTile();

            PropManager.PropInformation propInfo = obj.GetComponent<PropManager.PropInformation>();
            GridCell cell = obj.GetComponent<GridCell>();
            EntityManager.EntityInformation entityInfo = obj.GetComponent<EntityManager.EntityInformation>();

            //If we are moving a cell, then we want to keep track of the current grid cell info for undos/redos
            if (cell != null)
                oldEditorTile.Set(cell.tileInfo, cell.followsRule, cell.ruleID);

            //Set the movement based on whether we are moving a prop/entity or grid
            if (propInfo != null || entityInfo != null)
            {
                oldX = obj.transform.position.x;
                oldY = obj.transform.position.y;
            }
            else if (cell != null)
            {
                oldX = cell.x;
                oldY = cell.y;
            }


            object returnedObj = levelManager.levelEditManager.MoveCell(x, y, obj, selectedObjectsList.Any(t => t.GetComponent<GridCell>()));
            if (returnedObj != null)
            {
                //Add the new grid cell to the selected list, and add necessary history info
                if (returnedObj is GridCell)
                {
                    newCell = (GridCell)returnedObj;
                    newX = newCell.x;
                    newY = newCell.y;

                    //int indexOld = selectedObjectsList.IndexOf(obj);
                    toKeep.Add(newCell.gameObject);
                    oldObj.Add(obj);

                    movedObjects.Add(new object[] { cell, newCell, oldEditorTile });
                }
            }
            else
            {
                toKeep.Add(obj);
                newX = obj.gameObject.transform.position.x;
                newY = obj.gameObject.transform.position.y;

                if (propInfo != null)
                    movedObjects.Add(new object[] { propInfo, oldX, oldY, newX, newY });

                else if (entityInfo != null)
                    movedObjects.Add(new object[] { entityInfo, oldX, oldY, newX, newY });
            }
        }

        foreach (GameObject obj in oldObj)
            if (!toKeep.Contains(obj))
                levelManager.levelEditManager.Delete(obj);

        selectedObjectsList.Clear();
        selectedObjectsList.AddRange(toKeep);

        if (movedObjects.Count > 0)
            levelManager.levelEditManager.historyManager.MovedObjects(movedObjects);
    }

    void SelectDelete()
    {
        List<object[]> deletedInfo = new List<object[]>();

        foreach (GameObject obj in selectedObjectsList)
        {
            if (obj.GetComponent<PropManager.PropInformation>() != null)
            {
                deletedInfo.Add(new object[] { obj.GetComponent<PropManager.PropInformation>() });
            }
            else if (obj.GetComponent<GridCell>() != null)
            {
                GridCell cell = obj.GetComponent<GridCell>();
                deletedInfo.Add(new object[] { cell, new EditorTile(cell.tileInfo, cell.followsRule, cell.ruleID) });
            }
            else if (obj.GetComponent<EntityManager.EntityInformation>() != null)
            {
                deletedInfo.Add(new object[] { obj.GetComponent<EntityManager.EntityInformation>() });
            }

            levelManager.levelEditManager.Delete(obj);
        }

        if (deletedInfo.Count > 0)
            levelManager.levelEditManager.historyManager.Deleted(deletedInfo);
        selectedObjectsList.Clear();
    }

    #endregion

    #region GUI
    //Whenever you select a tile, using the select tool you will notice a square surronds it, the following code adds this functionality

    public void OnGUI()
    {
        InitStyles();

        if (selectedObjectsList.Count == 0) return;

        foreach (GameObject obj in selectedObjectsList)
        {
            Vector3[] pts = new Vector3[8];
            Bounds b = obj.GetComponent<SpriteRenderer>().bounds;
            Camera cam = Camera.main;

            //The object is behind us
            if (cam.WorldToScreenPoint(b.center).z < 0) return;

            //All 8 vertices of the bounds
            pts[0] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y + b.extents.y, b.center.z + b.extents.z));
            pts[1] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y + b.extents.y, b.center.z - b.extents.z));
            pts[2] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y - b.extents.y, b.center.z + b.extents.z));
            pts[3] = cam.WorldToScreenPoint(new Vector3(b.center.x + b.extents.x, b.center.y - b.extents.y, b.center.z - b.extents.z));
            pts[4] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y + b.extents.y, b.center.z + b.extents.z));
            pts[5] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y + b.extents.y, b.center.z - b.extents.z));
            pts[6] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y - b.extents.y, b.center.z + b.extents.z));
            pts[7] = cam.WorldToScreenPoint(new Vector3(b.center.x - b.extents.x, b.center.y - b.extents.y, b.center.z - b.extents.z));

            //Get them in GUI space
            for (int i = 0; i < pts.Length; i++) pts[i].y = Screen.height - pts[i].y;

            //Calculate the min and max positions
            Vector3 min = pts[0];
            Vector3 max = pts[0];
            for (int i = 1; i < pts.Length; i++)
            {
                min = Vector3.Min(min, pts[i]);
                max = Vector3.Max(max, pts[i]);
            }

            //Construct a rect of the min and max positions and apply some margin
            Rect r = Rect.MinMaxRect(min.x, min.y, max.x, max.y);

            //Render the box
            GUI.Box(r, "", currentStyle);
        }
    }

    private void InitStyles()
    {
        if (currentStyle == null)
        {
            currentStyle = new GUIStyle(GUI.skin.box);
            currentStyle.normal.background = MakeTex(2, 2, new Color(1f, 0.5f, 1f, 0.5f));
            currentStyle.hover.background = MakeTex(2, 2, new Color(1f, 0.5f, 1f, 0.5f));
        }
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; ++i)
        {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }
    #endregion
}
