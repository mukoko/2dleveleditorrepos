﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


/// <summary>
/// Controls the Undo/Redo system
/// </summary>
public class HistoryManager
{

    private List<History> _history;
    private History _currentHistory;

    private int _topIndex = -1;
    private LevelManager _levelManager;

    public bool saved { get; set; }

    public HistoryManager(LevelManager levelManager)
    {
        _levelManager = levelManager;
        _history = new List<History>();
        saved = true;
    }

    /// <summary>
    /// Whenever you add a new entry to the history list
    /// Set saved to false
    /// Permanetly remove an redo from the current index onwards
    /// </summary>
    private void AddHistoryEntry(History history)
    {
        saved = false;
        if (_history.Count > 0 && _history.Count > _topIndex + 1)
        {
            List<History> permDelete = _history.GetRange(_topIndex + 1, _history.Count - _topIndex - 1);
            DeleteHistory(permDelete);
            _history.RemoveRange(_topIndex + 1, _history.Count - _topIndex - 1);
        }

        _history.Add(history);
        _topIndex += 1;
    }

    /// <summary>
    /// Delete permanetly
    /// </summary>
    private void DeleteHistory(List<History> collection)
    {
        foreach (History entry in collection)
        {
            if (entry is Delete)
            {
                Delete d = (Delete)entry;
                if (d.changes[0] is PropManager.PropInformation)
                {
                    PropManager.PropInformation i = (PropManager.PropInformation)d.changes[0];
                    _levelManager.propManager.toDelete.Remove(i);
                    GameObject.Destroy(i.gameObject);
                }
                else if (d.changes[0] is EntityManager.EntityInformation)
                {
                    EntityManager.EntityInformation entityInformation = (EntityManager.EntityInformation)d.changes[0];
                    _levelManager.entityManager.toDelete.Remove(entityInformation);
                    GameObject.Destroy(entityInformation.gameObject);
                }
            }
        }
    }

    #region Undo
    public void Undo()
    {
        if (_history.Count == 0 || _topIndex < 0)
            return;

        saved = false;

        _currentHistory = _history[_topIndex];
        _topIndex -= 1;

        //Since each action is added to the history through a class which is a child of History, we can apply individual methods to each one of those actions
        Type type = _currentHistory.GetType();

        switch (type.Name)
        {
            case "PaintTile":
                UndoPaintTile();
                break;

            case "PaintMultiTiles":
                UndoPaintMultiTiles();
                break;

            case "PaintProp":
                PaintProp pProp = (PaintProp)_currentHistory;
                _levelManager.propManager.RemoveProp((PropManager.PropInformation)pProp.changes[0]);
                break;

            case "PaintEntity":
                PaintEntity pEntity = (PaintEntity)_currentHistory;
                _levelManager.entityManager.RemoveEntity((EntityManager.EntityInformation)pEntity.changes[0]);
                break;

            case "SelectObject":
                UndoSelectObjects();
                break;

            case "Delete":
                UndoDelete();
                break;

            case "Moved":
                UndoMoved();
                break;

            case "IncreaseLayerIndex":
                UndoIncreaseLIndex();
                break;

            case "DecreaseLayerIndex":
                UndoDecreaseLIndex();
                break;
        }
    }

    void UndoPaintTile()
    {
        PaintTile pTile = (PaintTile)_currentHistory;
        GridCell cell = (GridCell)pTile.changes[0];
        EditorMenu.EditorTile oldTileInfo = (EditorMenu.EditorTile)pTile.changes[1];

        //If the tile was previously empty, then set the tile to empty
        if (oldTileInfo.tileInfo == null)
            _levelManager.gridManager.RemoveCell(cell);
        //Otherwise set it to the previous tileinfo
        else cell.ChangeCellTile(oldTileInfo.tileInfo, oldTileInfo.followRules, oldTileInfo.ruleid);
    }

    void UndoPaintMultiTiles()
    {
        PaintMultiTiles pMTiles = (PaintMultiTiles)_currentHistory;
        List<EditorMenu.TileOldInfoPair> cellPairInfo = (List<EditorMenu.TileOldInfoPair>)pMTiles.changes[0];
        foreach (EditorMenu.TileOldInfoPair info in cellPairInfo)
        {
            if (info.oldEditorTile.tileInfo == null)
                _levelManager.gridManager.RemoveCell(info.cell);
            else info.cell.ChangeCellTile(info.oldEditorTile.tileInfo, info.oldEditorTile.followRules, info.oldEditorTile.ruleid);
        }
    }

    void UndoSelectObjects()
    {
        SelectObject sO = (SelectObject)_currentHistory;
        List<GameObject> list = (List<GameObject>)sO.changes[0];

        EditorMenu.Instance.selectedObjectsList.Clear();
        EditorMenu.Instance.selectedObjectsList.AddRange(list);
    }

    void UndoDelete()
    {
        Delete d = (Delete)_currentHistory;
        List<object[]> dList = (List<object[]>)d.changes[0];

        foreach (object[] dobj in dList)
        {
            if (dobj[0] is PropManager.PropInformation)
            {
                PropManager.PropInformation info = (PropManager.PropInformation)dobj[0];
                _levelManager.propManager.toDelete.Remove(info);
                _levelManager.propManager.props.Add(info);
                info.gameObject.SetActive(true);
                EditorMenu.Instance.selectedObjectsList.Add(info.gameObject);

            }
            else if (dobj[0] is GridCell)
            {
                GridCell cInfo = (GridCell)dobj[0];
                EditorMenu.EditorTile e = (EditorMenu.EditorTile)dobj[1];
                cInfo.ChangeCellTile(e.tileInfo, e.followRules, e.ruleid);
                EditorMenu.Instance.selectedObjectsList.Add(cInfo.gameObject);
            }
            else if (dobj[0] is EntityManager.EntityInformation)
            {
                EntityManager.EntityInformation info = (EntityManager.EntityInformation)dobj[0];
                _levelManager.entityManager.toDelete.Remove(info);
                _levelManager.entityManager.entities.Add(info);
                info.gameObject.SetActive(true);
                EditorMenu.Instance.selectedObjectsList.Add(info.gameObject);
            }

        }
    }

    void UndoMoved()
    {
        Moved m = (Moved)_currentHistory;
        List<object[]> changes = (List<object[]>)m.changes[0];

        List<object[]> gridChanges = new List<object[]>();
        foreach (object[] obj in changes)
        {
            if (obj[0] is GridCell)
                gridChanges.Add(obj);
        }

        List<GridCell> toRemove = new List<GridCell>();
        foreach (object[] obj in changes)
        {
            bool removeNewCell = true;
            if (obj[0] is PropManager.PropInformation)
            {
                PropManager.PropInformation info = (PropManager.PropInformation)obj[0];
                info.gameObject.transform.position = new Vector3((float)obj[1], (float)obj[2]);
            }
            else if (obj[0] is EntityManager.EntityInformation)
            {
                EntityManager.EntityInformation info = (EntityManager.EntityInformation)obj[0];
                info.gameObject.transform.position = new Vector3((float)obj[1], (float)obj[2]);
            }
            else if (obj[0] is GridCell)
            {
                foreach (object[] gCObj in gridChanges)
                {
                    if (gCObj != obj && (GridCell)gCObj[0] == (GridCell)obj[1])
                    {
                        removeNewCell = false;
                        break;
                    }
                }

                GridCell oldCell = (GridCell)obj[0];
                GridCell newCell = (GridCell)obj[1];
                EditorMenu.EditorTile eT = (EditorMenu.EditorTile)obj[2];

                oldCell.ChangeCellTile(eT.tileInfo, eT.followRules, eT.ruleid);

                if (removeNewCell) toRemove.Add(newCell);

                EditorMenu.Instance.selectedObjectsList.Remove(newCell.gameObject);
                EditorMenu.Instance.selectedObjectsList.Add(oldCell.gameObject);
            }
        }

        foreach (GridCell cellToRemove in toRemove)
            _levelManager.gridManager.RemoveCell(cellToRemove);
    }

    void UndoIncreaseLIndex()
    {
        IncreaseLayerIndex iLI = (IncreaseLayerIndex)_currentHistory;
        foreach (GameObject obj in (List<GameObject>)iLI.changes[0])
        {
            if (obj.GetComponent<PropManager.PropInformation>() != null)
                _levelManager.propManager.DecreaseIndex(obj.GetComponent<PropManager.PropInformation>());
            else if (obj.GetComponent<GridCell>() != null)
                _levelManager.gridManager.DecreaseIndex(obj.GetComponent<GridCell>());
            else if (obj.GetComponent<EntityManager.EntityInformation>() != null)
                _levelManager.entityManager.DecreaseIndex(obj.GetComponent<EntityManager.EntityInformation>());
        }
    }

    void UndoDecreaseLIndex()
    {
        DecreaseLayerIndex dLI = (DecreaseLayerIndex)_currentHistory;
        foreach (GameObject obj in (List<GameObject>)dLI.changes[0])
        {
            if (obj.GetComponent<PropManager.PropInformation>() != null)
                _levelManager.propManager.IncreaseIndex(obj.GetComponent<PropManager.PropInformation>());
            else if (obj.GetComponent<GridCell>() != null)
                _levelManager.gridManager.IncreaseIndex(obj.GetComponent<GridCell>());
            else if (obj.GetComponent<EntityManager.EntityInformation>() != null)
                _levelManager.entityManager.IncreaseIndex(obj.GetComponent<EntityManager.EntityInformation>());
        }
    }
    #endregion

    #region Redo
    public void Redo()
    {
        if (_topIndex == _history.Count - 1)
            return;

        saved = false;
        _topIndex += 1;

        _currentHistory = _history[_topIndex];
        Type type = _currentHistory.GetType();

        switch (type.Name)
        {
            case "PaintTile":
                RedoPaintTile();
                break;

            case "PaintMultiTiles":
                RedoPaintMultiTiles();
                break;

            case "PaintProp":
                RedoPaintProp();
                break;

            case "PaintEntity":
                RedoPaintEntity();
                break;

            case "SelectObject":
                RedoSelectObject();
                break;

            case "Delete":
                RedoDelete();
                break;

            case "Moved":
                RedoMoved();
                break;

            case "IncreaseLayerIndex":
                RedoIncreaseLIndex();
                break;

            case "DecreaseLayerIndex":
                RedoDecreaseLIndex();
                break;
        }
    }

    void RedoPaintTile()
    {
        PaintTile pTile = (PaintTile)_currentHistory;
        GridCell cell = (GridCell)pTile.changes[0];
        EditorMenu.EditorTile newTileInfo = (EditorMenu.EditorTile)pTile.changes[2];

        cell.ChangeCellTile(newTileInfo.tileInfo, newTileInfo.followRules, newTileInfo.ruleid);
    }

    void RedoPaintMultiTiles()
    {
        PaintMultiTiles pMTiles = (PaintMultiTiles)_currentHistory;
        List<EditorMenu.TileOldInfoPair> cellPairInfo = (List<EditorMenu.TileOldInfoPair>)pMTiles.changes[0];
        foreach (EditorMenu.TileOldInfoPair info in cellPairInfo)
            info.cell.ChangeCellTile(info.newEditorTile.tileInfo, info.newEditorTile.followRules, info.newEditorTile.ruleid);
    }

    void RedoPaintProp()
    {
        PaintProp pProp = (PaintProp)_currentHistory;
        PropManager.PropInformation p = (PropManager.PropInformation)pProp.changes[0];
        _levelManager.propManager.toDelete.Remove(p);
        p.gameObject.SetActive(true);
    }

    void RedoPaintEntity()
    {
        PaintEntity pEntity = (PaintEntity)_currentHistory;
        EntityManager.EntityInformation e = (EntityManager.EntityInformation)pEntity.changes[0];
        _levelManager.entityManager.toDelete.Remove(e);
        e.gameObject.SetActive(true);
    }

    void RedoSelectObject()
    {
        SelectObject sO = (SelectObject)_currentHistory;
        List<GameObject> list = (List<GameObject>)sO.changes[1];

        EditorMenu.Instance.selectedObjectsList.Clear();
        EditorMenu.Instance.selectedObjectsList.AddRange(list);
    }

    void RedoDelete()
    {
        Delete d = (Delete)_currentHistory;
        List<object[]> dList = (List<object[]>)d.changes[0];

        foreach (object[] dobj in dList)
        {

            if (dobj[0] is PropManager.PropInformation)
            {
                PropManager.PropInformation info = (PropManager.PropInformation)dobj[0];
                _levelManager.levelEditManager.Delete(info.gameObject);
                EditorMenu.Instance.selectedObjectsList.Remove(info.gameObject);

            }
            else if (dobj[0] is GridCell)
            {
                GridCell cInfo = (GridCell)dobj[0];
                _levelManager.levelEditManager.Delete(cInfo.gameObject);
                EditorMenu.Instance.selectedObjectsList.Remove(cInfo.gameObject);
            }
            else if (dobj[0] is EntityManager.EntityInformation)
            {
                EntityManager.EntityInformation info = (EntityManager.EntityInformation)dobj[0];
                _levelManager.levelEditManager.Delete(info.gameObject);
                EditorMenu.Instance.selectedObjectsList.Remove(info.gameObject);
            }
        }
    }

    void RedoMoved()
    {
        Moved m = (Moved)_currentHistory;
        List<object[]> changes = (List<object[]>)m.changes[0];

        List<object[]> gridChanges = new List<object[]>();
        foreach (object[] obj in changes)
        {
            if (obj[0] is GridCell)
                gridChanges.Add(obj);
        }

        List<GridCell> toRemove = new List<GridCell>();
        foreach (object[] obj in changes)
        {
            bool removeOldCell = true;
            if (obj[0] is PropManager.PropInformation)
            {
                PropManager.PropInformation info = (PropManager.PropInformation)obj[0];
                info.gameObject.transform.position = new Vector3((float)obj[3], (float)obj[4]);
            }
            else if (obj[0] is EntityManager.EntityInformation)
            {
                EntityManager.EntityInformation info = (EntityManager.EntityInformation)obj[0];
                info.gameObject.transform.position = new Vector3((float)obj[3], (float)obj[4]);
            }
            else if (obj[0] is GridCell)
            {
                foreach (object[] gCObj in gridChanges)
                {
                    if (gCObj != obj && (GridCell)gCObj[1] == (GridCell)obj[0])
                    {
                        removeOldCell = false;
                        break;
                    }
                }

                GridCell oldCell = (GridCell)obj[0];
                GridCell newCell = (GridCell)obj[1];
                EditorMenu.EditorTile eT = (EditorMenu.EditorTile)obj[2];

                newCell.ChangeCellTile(eT.tileInfo, eT.followRules, eT.ruleid);

                if (removeOldCell) toRemove.Add(oldCell);

                EditorMenu.Instance.selectedObjectsList.Remove(oldCell.gameObject);
                EditorMenu.Instance.selectedObjectsList.Add(newCell.gameObject);
            }
        }

        foreach (GridCell cellToRemove in toRemove)
            _levelManager.gridManager.RemoveCell(cellToRemove);
    }

    void RedoIncreaseLIndex()
    {
        IncreaseLayerIndex iLI = (IncreaseLayerIndex)_currentHistory;
        foreach (GameObject obj in (List<GameObject>)iLI.changes[0])
        {
            if (obj.GetComponent<PropManager.PropInformation>() != null)
                _levelManager.propManager.IncreaseIndex(obj.GetComponent<PropManager.PropInformation>());
            else if (obj.GetComponent<GridCell>() != null)
                _levelManager.gridManager.IncreaseIndex(obj.GetComponent<GridCell>());
            else if (obj.GetComponent<EntityManager.EntityInformation>() != null)
                _levelManager.entityManager.IncreaseIndex(obj.GetComponent<EntityManager.EntityInformation>());
        }
    }

    void RedoDecreaseLIndex()
    {
        DecreaseLayerIndex dLI = (DecreaseLayerIndex)_currentHistory;
        foreach (GameObject obj in (List<GameObject>)dLI.changes[0])
        {
            if (obj.GetComponent<PropManager.PropInformation>() != null)
                _levelManager.propManager.DecreaseIndex(obj.GetComponent<PropManager.PropInformation>());
            else if (obj.GetComponent<GridCell>() != null)
                _levelManager.gridManager.DecreaseIndex(obj.GetComponent<GridCell>());
            else if (obj.GetComponent<EntityManager.EntityInformation>() != null)
                _levelManager.entityManager.DecreaseIndex(obj.GetComponent<EntityManager.EntityInformation>());
        }
    }
    #endregion

    #region History Types
    private class History
    {
        public object[] changes { get; set; }
    }

    private class SelectObject : History
    {
        public SelectObject(List<GameObject> selectedObjects, List<GameObject> nowSelectedObjects)
        {
            changes = new object[] { selectedObjects, nowSelectedObjects };
        }
    }

    private class PaintProp : History
    {
        public PaintProp(PropManager.PropInformation prop)
        {
            changes = new object[] { prop };
        }
    }

    private class PaintTile : History
    {
        public PaintTile(GridCell cell, EditorMenu.EditorTile oldTile, EditorMenu.EditorTile newTile)
        {
            changes = new object[] { cell, oldTile, newTile };
        }
    }

    private class PaintEntity : History
    {
        public PaintEntity(EntityManager.EntityInformation entity)
        {
            changes = new object[] { entity };
        }
    }

    private class PaintMultiTiles : History
    {
        public PaintMultiTiles(List<EditorMenu.TileOldInfoPair> cellInfo)
        {
            changes = new object[] { cellInfo };
        }
    }

    private class Delete : History
    {
        public Delete(List<object[]> deletedObjects)
        {
            changes = new object[] { deletedObjects };
        }
    }

    private class Moved : History
    {
        public Moved(List<object[]> movedObjects)
        {
            changes = new object[] { movedObjects };
        }
    }

    private class DecreaseLayerIndex : History
    {
        public DecreaseLayerIndex(List<GameObject> selectedObjects)
        {
            changes = new object[] { selectedObjects };
        }
    }

    private class IncreaseLayerIndex : History
    {
        public IncreaseLayerIndex(List<GameObject> selectedObjects)
        {
            changes = new object[] { selectedObjects };
        }
    }
    #endregion

    #region History Addition Methods
    public void SelectedObject(List<GameObject> selectedObjects, List<GameObject> nowSelectedObjects)
    {
        AddHistoryEntry(new SelectObject(selectedObjects, nowSelectedObjects));
    }

    public void PaintedTile(GridCell cell, EditorMenu.EditorTile oldTile, EditorMenu.EditorTile newTile)
    {
        AddHistoryEntry(new PaintTile(cell, oldTile, newTile));
    }

    public void PaintedProp(PropManager.PropInformation prop)
    {
        AddHistoryEntry(new PaintProp(prop));
    }

    public void PaintedEntity(EntityManager.EntityInformation entity)
    {
        AddHistoryEntry(new PaintEntity(entity));
    }

    public void PaintedMultiTiles(List<EditorMenu.TileOldInfoPair> cellInfo)
    {
        AddHistoryEntry(new PaintMultiTiles(cellInfo));
    }

    public void Deleted(List<object[]> deletedObjects)
    {
        AddHistoryEntry(new Delete(deletedObjects));
    }

    public void MovedObjects(List<object[]> movedObjects)
    {
        AddHistoryEntry(new Moved(movedObjects));
    }

    public void DecreasedLayerIndex(List<GameObject> selectedObjects)
    {
        AddHistoryEntry(new DecreaseLayerIndex(selectedObjects));
    }

    public void IncreasedLayerIndex(List<GameObject> selectedObjects)
    {
        AddHistoryEntry(new IncreaseLayerIndex(selectedObjects));
    }
    #endregion
}