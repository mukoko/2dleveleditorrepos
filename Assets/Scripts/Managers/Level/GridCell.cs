﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GridCell : MonoBehaviour
{

    [SerializeField]
    private int _x;

    [SerializeField]
    private int _y;

    public int x { get { return _x; } }
    public int y { get { return _y; } }

    public int tileID { get; set; }
    public int ruleID { get; set; }
    public bool followsRule { get; set; }
    public bool hasCollider { get; set; }
    public int layerIndex { get; set; }

    public SpriteRenderer sRenderer { get; private set; }

    public GridManager gridManager;

    public TileManager.TileInformation tileInfo { get; set; }

    void Awake()
    {
        sRenderer = GetComponent<SpriteRenderer>();
        sRenderer.sortingOrder = layerIndex;
    }

    public void Initialise(int x, int y)
    {
        _x = x;
        _y = y;
    }

    public void ChangeLayer(int x)
    {
        layerIndex = x;
        sRenderer.sortingOrder = layerIndex;
    }

    public void ChangeCellTile(TileManager.TileInformation tileInfo, bool followRules, int ruleid)
    {
        this.tileInfo = tileInfo;
        followsRule = followRules;
        tileID = tileInfo.id;
        ruleID = ruleid;
        hasCollider = tileInfo.addCollider;

        if (!followsRule)
            sRenderer.sprite = tileInfo.GetRule(ruleid).sprite;
        else
        {
            ChangeTileSprite(_x, _y);
            RefreshNeighbourTileSprite(_x, _y);
        }
                    
    }

    public void ChangeTileSprite(int tileX, int tileY)
    {
        if (tileInfo == null)
            return;

        if (!followsRule)
            return;

        sRenderer.sprite = tileInfo.defaultSprite;
        if (!tileInfo.hasRulesets)
            return;

        //If a cell follows rules and has a rule set, then check the 8 neighbouring grid cells and returning whether they are of the same type or not (same ID) 
        string neighbourComposition = string.Empty;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x != 0 || y != 0)
                {
                    if (SameType(tileX + x, tileY + y))
                    {
                        neighbourComposition += 'T';
                    }
                    else
                    {
                        neighbourComposition += 'F';
                    }
                }
            }
        }

        //Match the neighbouring composition to the one of the tiles rule set
        foreach (TileManager.TileInformation.Rule ruleset in tileInfo.ruleset)
        {
            if (ruleset.rule.Equals("") || ruleset.indexes.Equals("")) continue;

            int[] indexes = new int[ruleset.indexes.Length];
            char[] indexesAsChars = ruleset.indexes.ToCharArray();

            for (int i = 0; i < ruleset.indexes.Length; i++)
            {
                indexes[i] = int.Parse(indexesAsChars[i].ToString());
            }

            //If we find a match then change to sprite to the correct rule sprite
            if (Check(neighbourComposition, ruleset.rule, indexes))
                sRenderer.sprite = ruleset.sprite;
        }
    }

    private bool Check(string neighbourComposition, string rule, int[] indexes)
    {
        bool proveFalse = true;
        for (int i = 0; i < indexes.Length; i++)
        {
            if (!neighbourComposition[indexes[i]].Equals(rule[i]))
            {
                proveFalse = false;
            }
        }

        return proveFalse;
    }

    //When the cell updates its sprite to match the rules, update the neighbouring ones to
    public void RefreshNeighbourTileSprite(int tileX, int tileY)
    {
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                int checkX = tileX + x;
                int checkY = tileY + y;

                gridManager.RefreshCell(checkX, checkY);

            }
        }
    }

    //Checks if a cell's tileinfo shares the same id as the this cells tileinfo id
    public bool SameType(int cellX, int cellY)
    {
        GridCell neighbour = gridManager.GetCell(cellX, cellY);
        if (neighbour != null)
            if (neighbour.tileInfo != null)
                return neighbour.tileInfo.id.Equals(tileInfo.id);

        return false;
    }
}