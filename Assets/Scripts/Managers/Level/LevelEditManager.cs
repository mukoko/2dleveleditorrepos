﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains all of the methods that can be called from the Editor Menu to affect the level.
/// These methods shouldn't be accessible in the actual level. So this class is only created when in the Edit Mode.
/// </summary>
public class LevelEditManager : MonoBehaviour
{
    private LevelManager _levelManager;
    public HistoryManager historyManager { get; private set; }

    public void Initialise(LevelManager levelManager)
    {
        _levelManager = levelManager;
        historyManager = new HistoryManager(_levelManager);
    }

    /// <summary>
    /// Updates the xml file for a level
    /// </summary>
    public void SerializeLevel()
    {
        _levelManager.xmlManager.Save();
    }

    public void SetBackground(TileManager.TileInformation tileInfo)
    {
        _levelManager.backgroundManager.SetBackground(tileInfo);
    }

    public PropManager.PropInformation AddProp(Vector2 mousePosition, TileManager.TileInformation tileInfo)
    {
        return _levelManager.propManager.AddProp(mousePosition, tileInfo);
    }

    public EntityManager.EntityInformation AddEntity(Vector2 mousePosition, TileManager.TileInformation tileInfo)
    {
        return _levelManager.entityManager.AddEntity(mousePosition, tileInfo);
    }

    public void AddTileToCell(GridCell cell, TileManager.TileInformation tileInfo, bool followsRules, int ruleid)
    {
        _levelManager.gridManager.AddTileToCell(cell, tileInfo, followsRules, ruleid);
    }

    /// <summary>
    /// Disable/Enable the grid
    /// </summary>
    public void DisplayGrid()
    {
        _levelManager.blankGridObject.SetActive(!_levelManager.blankGridObject.activeSelf);
    }


    public object MoveCell(int x, int y, GameObject toMove, bool incGridTile)
    {
        PropManager.PropInformation propInformation = toMove.GetComponent<PropManager.PropInformation>();
        EntityManager.EntityInformation entityInformation = toMove.GetComponent<EntityManager.EntityInformation>();
        GridCell gridCell = toMove.GetComponent<GridCell>();

        if (propInformation != null)
        {
            _levelManager.propManager.MoveProp(x, y, propInformation, incGridTile);
            return null;
        }

        else if (entityInformation != null)
        {
            _levelManager.entityManager.MoveEntity(x, y, entityInformation, incGridTile);
            return null;
        }

        else if (gridCell != null)
        {
            return _levelManager.gridManager.MoveCell(x, y, gridCell);
        }
        

        return null;
    }

    public void Delete(GameObject toDelete)
    {
        PropManager.PropInformation propInformation = toDelete.GetComponent<PropManager.PropInformation>();
        EntityManager.EntityInformation entityInformation = toDelete.GetComponent<EntityManager.EntityInformation>();
        GridCell gridCell = toDelete.GetComponent<GridCell>();

        if (propInformation != null)
            _levelManager.propManager.RemoveProp(propInformation);

        else if (entityInformation)
            _levelManager.entityManager.RemoveEntity(entityInformation);

        else if (gridCell != null)
            _levelManager.gridManager.RemoveCell(gridCell);
    }

    public void DecreaseLayerIndex(GameObject toDecrease)
    {
        PropManager.PropInformation propInformation = toDecrease.GetComponent<PropManager.PropInformation>();
        EntityManager.EntityInformation entityInformation = toDecrease.GetComponent<EntityManager.EntityInformation>();
        GridCell gridCell = toDecrease.GetComponent<GridCell>();

        if (propInformation != null)
            _levelManager.propManager.DecreaseIndex(propInformation);
        
        else if (entityInformation != null)
            _levelManager.entityManager.DecreaseIndex(entityInformation);

        else if (gridCell != null)
            _levelManager.gridManager.DecreaseIndex(gridCell);    
    }


    public void IncreaseLayerIndex(GameObject toIncrease)
    {
        PropManager.PropInformation propInformation = toIncrease.GetComponent<PropManager.PropInformation>();
        EntityManager.EntityInformation entityInformation = toIncrease.GetComponent<EntityManager.EntityInformation>();
        GridCell gridCell = toIncrease.GetComponent<GridCell>();

        if (propInformation != null)
            _levelManager.propManager.IncreaseIndex(propInformation);

        else if (entityInformation != null)
            _levelManager.entityManager.IncreaseIndex(entityInformation);

        else if (gridCell != null)
            _levelManager.gridManager.IncreaseIndex(gridCell);
    }
}
