﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class LevelPathHandler : Editor {

	[MenuItem("Tools/Level Path Handler/Writer")]
    static void WritePaths()
    {
        List<string> addedLevels = new List<string>();
        string path = "Assets/Resources/LevelPaths.txt";

        string levelsPath = "Assets/Resources/Levels";

        foreach (string directory in Directory.GetDirectories(levelsPath))
        {
            string[] levels = Directory.GetFiles(directory);
            if (levels.Length == 0)
                continue;

            foreach (string level in levels)
            {
                if (!addedLevels.Contains(level) && !level.Contains(".meta"))
                    addedLevels.Add(level);
            }
        }

        StreamWriter writer = new StreamWriter(path, false);
        foreach (string level in addedLevels)
            writer.WriteLine(level);
        writer.Close();

        AssetDatabase.ImportAsset(path);
        TextAsset asset = (TextAsset) Resources.Load("LevelPaths");

        Debug.Log(asset.text);
    }
}
