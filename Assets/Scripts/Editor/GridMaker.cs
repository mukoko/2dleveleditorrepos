﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GridMaker : EditorWindow {

    private int _rowCount;
    private int _columnCount;
    private GameObject _cellObject;

    [MenuItem("Tools/Grid Maker")]
    public static void ShowWindow()
    {
        GetWindow<GridMaker>("Grid Maker");
    }

    void OnGUI()
    {
        GUILayout.Label("Create Grid");
        GUILayout.Space(1f);

        _rowCount = EditorGUILayout.IntField("Row Count", _rowCount);
        _columnCount = EditorGUILayout.IntField("Column Count", _columnCount);
        _cellObject = (GameObject)EditorGUILayout.ObjectField("Cell Object", _cellObject, typeof(GameObject), true);


        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Create Grid"))
        {
            CreateGrid();
        }
    }

    void CreateGrid()
    {
        if (_rowCount <= 0 || _columnCount <= 0)
            return;

        for (int i = 0; i < _columnCount; i++)
        {
            for (int j = 0; j < _rowCount; j++)
            {
                GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(_cellObject);
                obj.transform.position = new Vector3(i * 0.16f, j * 0.16f);
                obj.name = "Grid Cell - (" + i + ", " + j + ")";
                obj.GetComponent<GridCell>().Initialise(i, j);
            }
        }
    }
}
