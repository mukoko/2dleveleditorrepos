﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BackgroundManager : MonoBehaviour
{
    private SpriteRenderer _renderer;

    public int layerIndex { get; set; }
    public int imageID { get; set; }

    void Awake()
    {
        layerIndex = 0;
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.sortingOrder = layerIndex;
    }

    public void SetBackground(TileManager.TileInformation tileInfo)
    {
        if (imageID == tileInfo.id) return;

        imageID = tileInfo.id;
        _renderer.sprite = tileInfo.defaultSprite;
        _renderer.sortingOrder = layerIndex;
    }
}
