﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The script that is attached to the level prefab.
/// This script manages all of the sub-components of a level (The layers, XML, History, Tiles)
/// </summary>
public class LevelManager : MonoBehaviour
{
               
    [SerializeField]
    private float _xBoundary = 3.55f / 2f;

    [SerializeField]
    private float _yBoundary = 1f;               

    public float xBoundaries { get { return _xBoundary; } }
    public float yBoundaries { get { return _yBoundary; } }

    public GameObject blankGridObject { get; private set; }

    public LevelEditManager levelEditManager { get; private set; }
    public XMLManager xmlManager { get; private set; }
    public BackgroundManager backgroundManager { get; private set; }
    public PropManager propManager { get; private set; }
    public GridManager gridManager { get; private set; }
    public EntityManager entityManager { get; private set; }

    public TileManager tileManager { get; private set; }

    void Awake()
    {
        blankGridObject = transform.GetChild(4).gameObject;

        tileManager = GetComponent<TileManager>();

        backgroundManager = transform.GetChild(0).GetComponent<BackgroundManager>();
        propManager = transform.GetChild(1).GetComponent<PropManager>();
        propManager.Initialise(_xBoundary, _yBoundary);

        gridManager = transform.GetChild(2).GetComponent<GridManager>();

        entityManager = transform.GetChild(3).GetComponent<EntityManager>();
        entityManager.Initialise(_xBoundary, _yBoundary);

        xmlManager = gameObject.AddComponent<XMLManager>();


        //TODO CHANGE TO IF IN EDITOR MODE.
        if (true)
        {
            levelEditManager = gameObject.AddComponent<LevelEditManager>();
            levelEditManager.Initialise(this);
        }

    }

    /// <summary>
    /// Called by the Editor Menu script when the editor is loaded
    /// Called immediately after Awake
    /// </summary>
    public void Initialise(string path, string name)
    {
        xmlManager.Initialise(this, path);
        xmlManager.Load();
    }
}