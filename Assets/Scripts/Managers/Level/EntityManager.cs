﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EntityManager : MonoBehaviour
{
    private float _xBoundary;
    private float _yBoundary;

    public class EntityInformation : MonoBehaviour
    {
        public int tileID { get; set; }
        public int layerIndex { get; set; }
        public bool hasCollider { get; set; }
    }

    public int defaultIndex { get; set; }
    public List<EntityInformation> entities;
    public List<EntityInformation> toDelete;

    void Awake()
    {
        defaultIndex = 3;
    }
    
    public void Initialise(float x, float y)
    {
        _xBoundary = x;
        _yBoundary = y;
    }

    public EntityInformation AddEntity(Vector2 location, TileManager.TileInformation tileInfo)
    {
        GameObject go = new GameObject();
        go.transform.position = location;

        SpriteRenderer renderer = go.AddComponent<SpriteRenderer>();
        renderer.sprite = tileInfo.defaultSprite;
        renderer.sortingOrder = defaultIndex;

        BoxCollider2D collider = go.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;

        Vector2 S = renderer.sprite.bounds.size;
        collider.size = S;

        EntityInformation pInfo = go.AddComponent<EntityInformation>();
        pInfo.tileID = tileInfo.id;
        pInfo.layerIndex = defaultIndex;
        pInfo.hasCollider = tileInfo.addCollider;

        go.transform.SetParent(transform);
        entities.Add(pInfo);

        return pInfo;
    }

    public void AddEntity(Vector2 location, TileManager.TileInformation tileInfo, int index, bool hasCollder)
    {
        GameObject go = new GameObject();
        go.transform.position = location;

        SpriteRenderer renderer = go.AddComponent<SpriteRenderer>();
        renderer.sprite = tileInfo.defaultSprite;
        renderer.sortingOrder = index;

        BoxCollider2D collider = go.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;

        Vector2 S = renderer.sprite.bounds.size;
        collider.size = S;

        EntityInformation pInfo = go.AddComponent<EntityInformation>();
        pInfo.tileID = tileInfo.id;
        pInfo.layerIndex = index;
        pInfo.hasCollider = hasCollder;

        go.transform.SetParent(transform);
        entities.Add(pInfo);
    }

    public void MoveEntity(int x, int y, EntityInformation propInfo, bool incGridTile)
    {
        Vector3 currentPosition = propInfo.gameObject.transform.position;
        float incValue = 0.01f;
        if (incGridTile) incValue = 0.16f;

        if (currentPosition.x + (x * incValue) > _xBoundary || currentPosition.x + (x * incValue) < -_xBoundary || currentPosition.y + (y * incValue) > _yBoundary || currentPosition.y + (y * incValue) < -_yBoundary) return;
        propInfo.gameObject.transform.position = new Vector3(currentPosition.x + (x * incValue), currentPosition.y + (y * incValue));
    }

    public void RemoveEntity(EntityInformation eInfo)
    {
        entities.Remove(eInfo);
        toDelete.Add(eInfo);

        eInfo.gameObject.SetActive(false);
    }

    public void DecreaseIndex(EntityInformation eInfo)
    {
        if (eInfo.layerIndex > defaultIndex)
        {
            eInfo.layerIndex -= 1;
            eInfo.gameObject.GetComponent<SpriteRenderer>().sortingOrder = eInfo.layerIndex;
        }
    }

    public void IncreaseIndex(EntityInformation eInfo)
    {
        eInfo.layerIndex += 1;
        eInfo.gameObject.GetComponent<SpriteRenderer>().sortingOrder = eInfo.layerIndex;
    }
}
